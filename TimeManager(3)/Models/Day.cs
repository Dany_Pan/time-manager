﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeManager_3_.Models
{
    public class Day
    {
        public int DayId { get; set; }
        public DateTime DayDate { get; set; }
        public string DayEvent { get; set; }
        public string DayNotification { get; set; }

        public virtual Notifications Notification { get; set; }
        public virtual ICollection<Event> Events { get; set; }
        public virtual ICollection<CashTransaction> CashTransactions { get; set; }
    }
}
