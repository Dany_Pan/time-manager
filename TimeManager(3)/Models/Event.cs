﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeManager_3_.Models
{
    public class Event
    {
        public int EventId { get; set; }
        public string EventName { get; set; }
        public DateTime EventDate { get; set; }
        public int EventImportance { get; set; }
        public DateTime EventDuration { get; set; }
        public string EventFormality { get; set; }
        public virtual Day Day { get; set; }
    }
}
