﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeManager_3_.Models
{
    public class CashTransaction
    {
        public int CashTransactionId { get; set; }
        public float CashTransactionIncome { get; set; }
        public float CashTransactionExpence { get; set; }
        public float CashTransactionDebt { get; set; }
        public float CashTransactionOwed { get; set; }
        public virtual Day Day { get; set; }
        public virtual Budget Budget { get; set; }
    }
}
