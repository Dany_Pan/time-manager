﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeManager_3_.Models
{
    public class Notifications
    {
        public string NotificationEvents { get; set; }
        public DateTime NotificationsDate { get; set; }
        public virtual Day Day { get; set; }
    }
}
