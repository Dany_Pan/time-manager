﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeManager_3_.Models
{
    public class Budget
    {
        public int BudgetId { get; set; }
        public float BudgetAvailable { get; set; }
        public virtual ICollection<CashTransaction> CashTransactions { get; set; }
    }
}
