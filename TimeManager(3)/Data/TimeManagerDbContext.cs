﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TimeManager_3_.Models;

namespace TimeManager_3_.Data
{
    public class TimeManagerDbContext : DbContext

    {
        public DbSet<Day> Days { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Budget> Budgets { get; set; }
        public DbSet<CashTransaction> CashTransactions { get; set; }
        public DbSet<Notifications> Notifications { get; set; }

        public ApplicationDbContext(DbContextOptions<TimeManagerDbContext> options)
            : base(options)
        {
        }
    }
}
